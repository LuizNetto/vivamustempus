import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {Homefeature, InitialFeature} from '../features';

const Stack = createNativeStackNavigator();

export const Routes = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Initial"
          component={InitialFeature.screens.Initial}
        />
        <Stack.Screen name="Home" component={Homefeature.screens.Home} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
