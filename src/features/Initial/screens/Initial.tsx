import React from 'react';
import styled from 'styled-components/native';
import {} from 'react-native-safe-area-context';

export const Initial = () => {
  return (
    <Container>
      <TextContainer>
        <Text>INITIAL SCREEN</Text>
      </TextContainer>
      <Button>
        <ButtonText>NEXT SCREEN</ButtonText>
      </Button>
    </Container>
  );
};

const Container = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  background-color: #ffffff;
`;

const TextContainer = styled.View`
  border: 1px solid #000000;
  padding: 16px;
  border-radius: 6px;
`;

const Text = styled.Text`
  color: #000000;
  font-size: 24px;
  font-weight: 600;
`;

const Button = styled.TouchableOpacity`
  margin: 32px 0;
  background-color: #23a8d9;
  padding: 16px 32px;
  border-radius: 6px;
`;

const ButtonText = styled.Text`
  font-size: 15px;
  font-weight: 500;
  color: #ffffff;
`;
